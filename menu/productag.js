const mysql = require('mysql');
const config101 = require('../con/configg')

let config11 = Config.prototype;
function Config() {}


function get(req, res) { 
  
    let connection = config101.getConnection();
  
      connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          res.json( { code: 500, message: "Failed.", error_mgs: err } );
          return;
        }
        console.log('connected as id ' + connection.threadId);
  
        let query = "SELECT `id`, `product_id`, `tag_id`, `status`, `create_date`, `update_date` FROM `Product_tag`"
        connection.query(query, function (error, results, fields) {
          if (error) throw error;
          console.log(results);
          res.json(results);
        });
  
        connection.end(function(err) {});
      });
  };

function post(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Product_tag`(`product_id`, `tag_id`, `status`) VALUES (?,?,?);";
          let param = [raw_json["product_id"],raw_json["tag_id"],raw_json["status"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };

function put(req, res) { 
    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "UPDATE `Product_tag` SET `status`=? WHERE `id`=?;";
          let param = [raw_json["status"] , raw_json["id"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
    
  };

function deletee(req, res) { 
    let raw_json = req.body;
    console.log(raw_json);
  
    let connection = config101.getConnection();
  
    connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        res.json( { code: 500, message: "Failed.", error_mgs: err } );
        return;
      }
  
      let query = "DELETE FROM `Product_tag` WHERE `id`=?;";
      let param = [ raw_json["id"]];
  
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {
        if(error) {
          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          console.log(error.stack);
        }else {
          res.json( { code: 200, message: "Success." } );
        }
      });
  
      connection.end(function(err) {});
    });    
  
  };


  config11.sss =  function initgrade(app){

    app.get('/productag', get)
    app.post('/productag', post)
    app.put('/productag', put)
    app.delete('/productag', deletee)
  
  }


module.exports = config11;