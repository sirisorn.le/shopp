const mysql = require('mysql');
const config101 = require('../con/configg')

let config6 = Config.prototype;
function Config() {}


function get(req, res) { 
  
    let connection = config101.getConnection();
  
      connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          res.json( { code: 500, message: "Failed.", error_mgs: err } );
          return;
        }
        console.log('connected as id ' + connection.threadId);
  
        let query = "SELECT `id`, `photo`, `review_id`, `status`, `create_date`, `update_date` FROM `Gallery_review`"
        connection.query(query, function (error, results, fields) {
          if (error) throw error;
          console.log(results);
          res.json(results);
        });
  
        connection.end(function(err) {});
      });
  };

function post(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Gallery_review`(`photo`, `review_id`, `status`) VALUES VALUES (?,?,?);";
          let param = [raw_json["photo"],raw_json["review_id"], raw_json["status"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };

function put(req, res) { 
    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "UPDATE `Gallery_review` SET `status`=? WHERE `id`=?;";
          let param = [raw_json["status"] , raw_json["id"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
    
  };

function deletee(req, res) { 
    let raw_json = req.body;
    console.log(raw_json);
  
    let connection = config101.getConnection();
  
    connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        res.json( { code: 500, message: "Failed.", error_mgs: err } );
        return;
      }
  
      let query = "DELETE FROM `Gallery_review` WHERE `id`=?;";
      let param = [ raw_json["id"]];
  
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {
        if(error) {
          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          console.log(error.stack);
        }else {
          res.json( { code: 200, message: "Success." } );
        }
      });
  
      connection.end(function(err) {});
    });    
  
  };


  config6.sss =  function initgrade(app){

    app.get('/gallery_review', get)
    app.post('/gallery_review', post)
    app.put('/gallery_review', put)
    app.delete('/gallery_review', deletee)
  
  }


module.exports = config6;