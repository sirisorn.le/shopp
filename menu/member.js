const mysql = require('mysql');
const config101 = require('../con/configg')

let config7 = Config.prototype;
function Config() {}


function get(req, res) { 
  
    let connection = config101.getConnection();
  
      connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          res.json( { code: 500, message: "Failed.", error_mgs: err } );
          return;
        }
        console.log('connected as id ' + connection.threadId);
  
        let query = "SELECT `id`, `name`, `user`, `password`, `photo`, `email`, `tel`, `status`, `create_date`, `update_date`, `address` FROM `Member`"
        connection.query(query, function (error, results, fields) {
          if (error) throw error;
          console.log(results);
          res.json(results);
        });
  
        connection.end(function(err) {});
      });
  };

function post(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Member`(`name`, `user`, `password`, `photo`, `email`, `tel`, `status`, `address`)  VALUES (?,?,?,?,?,?,?,?);";
          let param = [raw_json["name"],raw_json["user"], raw_json["password"], raw_json["photo"], raw_json["email"], raw_json["tel"], raw_json["status"], raw_json["address"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };

function put(req, res) { 
    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "UPDATE `Member` SET `status`=? WHERE `id`=?;";
          let param = [raw_json["status"] , raw_json["id"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
    
  };

  function postlogin(req, res) { 

    let raw_json = req.body;
    console.log(raw_json);

    let connection = config101.getConnection();

    connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          return;
        }
  
        let query = "SELECT `id`, `name`, `user`, `password` FROM `Member` WHERE `user` =? AND `password`=? "
        let param = [raw_json["user"], raw_json["password"]];
        console.log("beparam",param);
        connection.query({
          sql: query,
          timeout: 2000, // 2s
          values: param
        }, function (error, results, fields) {

            if(error) {
                connection.end(function(err) {});
                res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
            } else {
              console.log("results.length",results.length)
              console.log("results",results)
             
              
              console.log("xx")
                if(results.length > 0) {
                 
                      console.log("aaaa")
                    let now = new Date();
                    let query2 = "UPDATE `Member` SET `login_date`= ?, `status`=2 WHERE `id` =?;";
                    let param2 = [now, results[0].id];
                    
                    connection.query({
                        sql: query2,
                        timeout: 2000, // 2s
                        values: param2
                       
                    }, function (error, results2, fields) {

                        if(error) {
                            connection.end(function(err) {});
                            res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
                        } else {
                          now = now.toLocaleString("th-TH", {timeZone: "Asia/Bangkok"});
                            res.json( { code: 200, message: "Success.", res: results, time: now } );
                        }
                    });

                



              }

              // }
              else {
                connection.end(function(err) {});
                res.json( { code: 500, message: "Login Failed."})
            }
            }
            

        
          //       connection.end(function(err) {});
  
          //     }
          //   });
          // }
            //ff
        });
        
  
        
      });  
};

function postlogout(req, res) { 

  let raw_json = req.body;
  console.log(raw_json);

  let connection = config101.getConnection();

  connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        return;
      }

      let query = "SELECT `user` FROM `Member` WHERE `id` =? and `status`=2"
      let param = [raw_json["id"]];
      console.log("beparam",param);
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {

          if(error) {
              connection.end(function(err) {});
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          } else {
              if(results.length > 0) {

                  let now = new Date();
                  let query2 = "UPDATE `Member` SET `logout_date`= ?, `status`=1 WHERE `id` =?;";
                  let param2 = [now, results[0].id];
                  
                  connection.query({
                      sql: query2,
                      timeout: 2000, // 2s
                      values: param2
                     
                  }, function (error, results2, fields) {

                      if(error) {
                          connection.end(function(err) {});
                          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
                      } else {
                        now = now.toLocaleString("th-TH", {timeZone: "Asia/Bangkok"});
                          res.json( { code: 200, message: "logout.", res: results, time: now } );
                      }
                  });

              } else {
                  connection.end(function(err) {});
                  res.json( { code: 500, message: "Login Failed."})
              }
          }

        
      });

      
    });  
};

function deletee(req, res) { 
    let raw_json = req.body;
    console.log(raw_json);
  
    let connection = config101.getConnection();
  
    connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        res.json( { code: 500, message: "Failed.", error_mgs: err } );
        return;
      }
  
      let query = "DELETE FROM `Member` WHERE `id`=?;";
      let param = [ raw_json["id"]];
  
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {
        if(error) {
          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          console.log(error.stack);
        }else {
          res.json( { code: 200, message: "Success." } );
        }
      });
  
      connection.end(function(err) {});
    });    
  
  };


  config7.sss =  function initgrade(app){

    app.get('/member', get)
    app.post('/member', post)
    app.put('/member', put)
    app.delete('/member', deletee)
  
  }


module.exports = config7;