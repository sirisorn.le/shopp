const mysql = require('mysql');
const config101 = require('../con/configg')

let config4 = Config.prototype;
function Config() {}


function get(req, res) { 
  
    let connection = config101.getConnection();
  
      connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          res.json( { code: 500, message: "Failed.", error_mgs: err } );
          return;
        }
        console.log('connected as id ' + connection.threadId);
  
        let query = "SELECT `id`, `name`, `discount_code`, `disprice`, `discount_percent`, `status`, `create_date`, `update_date` FROM `Discount`"
        connection.query(query, function (error, results, fields) {
          if (error) throw error;
          console.log(results);
          res.json(results);
        });
  
        connection.end(function(err) {});
      });
  };

function post(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Discount`(`name`, `discount_code`, `disprice`, `status`) VALUES (?,?,?,?);";
          let param = [raw_json["name"],raw_json["discount_code"],raw_json["disprice"], raw_json["status"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };

function post2(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Discount`(`name`, `discount_code`, `discount_percent`, `status`) VALUES (?,?,?,?);";
          let param = [raw_json["name"],raw_json["discount_code"],raw_json["discount_percent"], raw_json["status"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };  

function put(req, res) { 
    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "UPDATE `Discount` SET `status`=? WHERE `id`=?;";
          let param = [raw_json["status"] , raw_json["id"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
    
  };

function deletee(req, res) { 
    let raw_json = req.body;
    console.log(raw_json);
  
    let connection = config101.getConnection();
  
    connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        res.json( { code: 500, message: "Failed.", error_mgs: err } );
        return;
      }
  
      let query = "DELETE FROM `Discount` WHERE `id`=?;";
      let param = [ raw_json["id"]];
  
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {
        if(error) {
          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          console.log(error.stack);
        }else {
          res.json( { code: 200, message: "Success." } );
        }
      });
  
      connection.end(function(err) {});
    });    
  
  };


  config4.sss =  function initgrade(app){

    app.get('/discount', get)
    app.post('/disprice', post)
    app.post('/dispercent', post2)
    app.put('/discount', put)
    app.delete('/discount', deletee)
  
  }


module.exports = config4;