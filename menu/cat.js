const mysql = require('mysql');
const config101 = require('../con/configg')

let config2 = Config.prototype;
function Config() {}


function get(req, res) { 
  
    let connection = config101.getConnection();
  
      connection.connect(function(err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          res.json( { code: 500, message: "Failed.", error_mgs: err } );
          return;
        }
        console.log('connected as id ' + connection.threadId);
  
        let query = "SELECT `id`, `name`, `status`, `create_date`, `update_date` FROM `Category` "
        connection.query(query, function (error, results, fields) {
          if (error) throw error;
          console.log(results);
          res.json(results);
        });
  
        connection.end(function(err) {});
      });
  };

function post(req, res) { 

    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "INSERT INTO `Category`(`id`, `name`, `status`, ) VALUES (?,?,?);";
          let param = [raw_json["id"],raw_json["name"], raw_json["status"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
  
  };

function put(req, res) { 
    let raw_json = req.body;
        console.log(raw_json);
    
        let connection = config101.getConnection();
    
        connection.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            res.json( { code: 500, message: "Failed.", error_mgs: err } );
            return;
          }
    
          let query = "UPDATE `Category` SET `status`=? WHERE `id`=?;";
          let param = [raw_json["status"] , raw_json["id"]];
    
          connection.query({
            sql: query,
            timeout: 2000, // 2s
            values: param
          }, function (error, results, fields) {
            if(error) {
              res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
              console.log(error.stack);
            }else {
              res.json( { code: 200, message: "Success." } );
            }
          });
    
          connection.end(function(err) {});
        });    
    
  };

function deletee(req, res) { 
    let raw_json = req.body;
    console.log(raw_json);
  
    let connection = config101.getConnection();
  
    connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        res.json( { code: 500, message: "Failed.", error_mgs: err } );
        return;
      }
  
      let query = "DELETE FROM `Category` WHERE `id`=?;";
      let param = [ raw_json["id"]];
  
      connection.query({
        sql: query,
        timeout: 2000, // 2s
        values: param
      }, function (error, results, fields) {
        if(error) {
          res.json( { code: 500, message: "Failed.", error_mgs: error.stack } );
          console.log(error.stack);
        }else {
          res.json( { code: 200, message: "Success." } );
        }
      });
  
      connection.end(function(err) {});
    });    
  
  };


  config2.sss =  function initgrade(app){

    app.get('/cat', get)
    app.post('/cat', post)
    app.put('/cat', put)
    app.delete('/cat', deletee)
  
  }


module.exports = config2;