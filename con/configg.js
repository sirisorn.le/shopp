const mysql = require('mysql');
let config101 = Configg.prototype;

const fs = require('fs-extra')


function Configg() {}

config101.getConnection = function() {

  const packageObj = fs.readJsonSync('con/con.json')
  console.log(packageObj) // => 2.0.0

  return connection = mysql.createConnection({
    host     : packageObj.mysql_host,
    port     : packageObj.mysql_port,
    user     : packageObj.mysql_user,
    password : packageObj.mysql_password,
    database : packageObj.mysql_database
  });
}

module.exports = config101;